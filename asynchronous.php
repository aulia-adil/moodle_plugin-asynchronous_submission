<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: *");

    define('AJAX_SCRIPT', true);

    require_once(__DIR__ . '/../../../../config.php');
    require_once($CFG->dirroot . '/mod/quiz/locallib.php');

    require_sesskey();

    $record = new stdClass();
    $record->attemptid = required_param('attemptid',  PARAM_INT);
    $record->cmid = required_param("cmid", PARAM_INT);
    $record->courseid = required_param("courseid", PARAM_INT);
    $record->userid = required_param("userid", PARAM_INT);
    $record->activitycode = required_param("activitycode", PARAM_INT);
    $record->activity = required_param("activity", PARAM_TEXT);
    // $record->timestamp = round(microtime(true) * 1000);
    $record->timestamp = required_param("timestamp", PARAM_INT);

    $attemptobj = quiz_create_attempt_handling_errors($record->attemptid, $record->cmid);

    // Check login.
    require_login($attemptobj->get_course(), false, $attemptobj->get_cm());

    // Check that this attempt belongs to this user.
    if ($attemptobj->get_userid() != $USER->id) {
        throw new moodle_quiz_exception($attemptobj->get_quizobj(), 'notyourattempt');
    }

    // Check capabilities.
    if (!$attemptobj->is_preview_user()) {
        $attemptobj->require_capability('mod/quiz:attempt');
    }

    // If the attempt is already closed, send them to the review page.
    if ($attemptobj->is_finished()) {
        throw new moodle_quiz_exception($attemptobj->get_quizobj(),
                'attemptalreadyclosed', null, $attemptobj->review_url());
    }
    
    $DB->insert_record("asynchronous", $record);

    echo json_encode("success");