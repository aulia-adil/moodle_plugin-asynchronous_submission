<?php

define('CLI_SCRIPT', true);

require_once(__DIR__ . '/../../../../config.php');
require_once($CFG->dirroot . '/mod/quiz/locallib.php');
require_once($CFG->dirroot . '/vendor/autoload.php');

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface {

    protected $clients;

    public function __construct() {
        $this->clients = new \SplObjectStorage;
        echo 'server start.';
        
    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);
        echo "New connection! ({$conn->resourceId})\n";
    }

    public function onMessage(ConnectionInterface $from, $msg) {
        global $DB;
        $parameter = json_decode($msg, true);
        $sesskey = $parameter["sesskey"];
        $userid = $parameter["userid"];
        $data = $DB->get_record_sql("SELECT * FROM public.mdl_shared_sesskey WHERE userid=" . $userid . 
        " ORDER BY id DESC LIMIT 1");
        if ($sesskey != $data->sesskey) {
            print_error("invalidsesskey");
        }

        $from->attemptid = $parameter["attemptid"];
        $from->cmid = $parameter["cmid"];
        $from->courseid = $parameter["courseid"];
        $from->userid = $userid; 

        $record = new stdClass();
        $record->attemptid = $parameter["attemptid"];
        $record->cmid = $parameter["cmid"];
        $record->courseid = $parameter["courseid"];
        $record->userid = $userid;
        $record->activitycode = 5;
        $record->activity = "online";
        $record->timestamp = round(microtime(true) * 1000);

        $attemptobj = quiz_create_attempt_handling_errors($record->attemptid, $record->cmid);

        // Check that this attempt belongs to this user.
        if ($attemptobj->get_userid() != $userid) {
            throw new moodle_quiz_exception($attemptobj->get_quizobj(), 'notyourattempt');
        }

        $DB->insert_record("asynchronous", $record);
    }

    public function onClose(ConnectionInterface $conn) {
        global $DB;
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);

        $record = new stdClass();
        $record->attemptid = $conn->attemptid;
        $record->cmid = $conn->cmid;
        $record->courseid = $conn->courseid;
        $record->userid = $conn->userid;
        $record->activitycode = 5;
        $record->activity = "offline";
        $record->timestamp = round(microtime(true) * 1000);

        $DB->insert_record("asynchronous", $record);

        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->send($e->getMessage());
        $conn->close();
    }

}


$wsServer = new Ratchet\WebSocket\WsServer(new Chat);
$server = IoServer::factory(
                new HttpServer(
                        $wsServer
                ),
                $CFG->wsport
);

$wsServer->enableKeepAlive($server->loop, 1);

$server->run();



